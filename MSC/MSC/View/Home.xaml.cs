﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MSC.View;

namespace MSC
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Home : MasterDetailPage
    {
        public Home()
        {
            InitializeComponent();
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as HomeMenuItem;
            if (item == null)
                return;

            var page = (Page)Activator.CreateInstance(item.TargetType);
            page.Title = item.Title;

            switch (item.Id)
            {
                case 0:
                    IsPresented = false;
                    break;

                case 1:
                    Detail = new NavigationPage(new Login());
                    break;

                case 2:
                    Detail = new NavigationPage(new Login());
                    break;

                case 3:
                    Detail = new NavigationPage(new Login());
                    break;

                case 4:
                    Detail = new NavigationPage(new Login());
                    break;

                default:
                    break;
            }
            
            IsPresented = false;

            MasterPage.ListView.SelectedItem = null;
        }
    }
}