﻿using Android.Content;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using MSC.Droid.Renderer;
using MSC.Controls;

[assembly: ExportRenderer(typeof(EntryLoginEmail), typeof(EntryLoginEmailRender))]
namespace MSC.Droid.Renderer
{
    class EntryLoginEmailRender : EntryRenderer
    {
        public EntryLoginEmailRender(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.SetBackgroundResource(Resource.Drawable.SelectorEntry);
                Control.SetPadding(110, 30, 0, 30);
            }
        }
    }
}