﻿using Android.Content;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using MSC.Droid.Renderer;
using MSC.Controls;

[assembly: ExportRenderer(typeof(EntryLoginPassword),typeof(EntryLoginPasswordRender))]
namespace MSC.Droid.Renderer
{
    class EntryLoginPasswordRender : EntryRenderer
    {
        public EntryLoginPasswordRender(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.SetBackgroundResource(Resource.Drawable.SelectorEntry);
                Control.SetPadding(110, 30, 0, 30);
            }
        }
    }
}